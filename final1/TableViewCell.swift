//
//  TableViewCell.swift
//  final1
//
//  Created by Aashi Mahajan on 31/01/23.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var LabelName: UILabel!
    @IBOutlet weak var ImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(with data: Data)
    {
        LabelName.text = data.title
        ImageView.image = data.image
    }
  

}
